#include "estado.h"

int Sig_Estado(int carro, int estado2){
    estados edosig;
    switch(estado2){
        case 0:
            switch(carro){
                case 0:
                    edosig = go_Norte;
                    break;
                case 1:
                    edosig = wait_Norte;
                    break;
                case 2:
                    edosig = go_Norte;
                    break;
                case 3:
                    edosig = wait_Norte;
                    break;
            }
            break;
        case 1:
            edosig = go_Este;
            break;
        case 2:
            switch(carro){
                case 0:
                    edosig = go_Este;
                    break;
                case 1:
                    edosig = go_Este;
                    break;
                case 2:
                    edosig = wait_Este;
                    break;
                case 3:
                    edosig = wait_Este;
                    break;
            }
            break;
        case 3:
            edosig = go_Norte;
            break;
    }
    
    return edosig;
}

void Estados_Actual(int estado){
    switch(estado){
        case 0:
            printf("goNorte");
            break;
        case 1:
            printf("waitNorte");
            break;
        case 2:
            printf("goEste");
            break;
        case 3:
            printf("waitEste");
            break;
    }
}
