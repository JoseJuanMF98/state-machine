#include "estado.h"
#include <gtest/gtest.h>

TEST(SquareRootTest, NomPrueba){

	ASSERT_EQ(go_Norte, Sig_Estado(No_Carro,go_Norte));
    ASSERT_EQ(go_Norte, Sig_Estado(Carro_Norte,go_Norte));
    ASSERT_EQ(wait_Norte, Sig_Estado(Carro_Este,go_Norte));
    ASSERT_EQ(wait_Norte, Sig_Estado(Ambos_Lados,go_Norte));

    ASSERT_EQ(go_Este, Sig_Estado(No_Carro,wait_Norte));
    ASSERT_EQ(go_Este, Sig_Estado(Carro_Este,wait_Norte));
    ASSERT_EQ(go_Este, Sig_Estado(Carro_Norte,wait_Norte));
    ASSERT_EQ(go_Este, Sig_Estado(Ambos_Lados,wait_Norte));

	ASSERT_EQ(go_Este, Sig_Estado(No_Carro,go_Este));
	ASSERT_EQ(go_Este, Sig_Estado(Carro_Este,go_Este));    
	ASSERT_EQ(wait_Este, Sig_Estado(Carro_Norte,go_Este));
	ASSERT_EQ(wait_Este, Sig_Estado(Ambos_Lados,go_Este));

	ASSERT_EQ(go_Norte, Sig_Estado(No_Carro,wait_Este));
	ASSERT_EQ(go_Norte, Sig_Estado(Carro_Este,wait_Este));
	ASSERT_EQ(go_Norte, Sig_Estado(Carro_Norte,wait_Este));
	ASSERT_EQ(go_Norte, Sig_Estado(Ambos_Lados,wait_Este));
   
}


int main(int argc, char **argv){
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
