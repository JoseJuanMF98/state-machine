#ifndef __ESTADO_H
#define __ESTADO_H

#ifdef __cplusplus
extern "C"{
#endif

#include <stdio.h>
#include <stdlib.h>

typedef enum{
    go_Norte,
    wait_Norte,
    go_Este,
    wait_Este
} estados;

typedef enum{
    No_Carro,
    Carro_Este,
    Carro_Norte,
    Ambos_Lados
} carros;

int Sig_Estado(int carro, int estado2);
void Estados_Actual(int estado); 

#ifdef __cplusplus
}

#endif

#endif
