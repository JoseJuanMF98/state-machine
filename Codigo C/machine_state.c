#include <stdio.h>
#include <stdlib.h>

int actual;

typedef enum{
    No_Carros,
    Carros_Este,
    Carros_Norte,
    Ambos_Lados
} Autos;

typedef enum{
    goNorte,
    waitNorte,
    goEste,
    waitEste
} Estados;

int main(){
   
    int array[5]={Ambos_Lados,Carros_Este,No_Carros,Carros_Norte,Ambos_Lados}; //Definimos todas los posibles estados en un array para un recorrido total
    Estados edo_sig; 
    edo_sig=0; //Incializamos en el Estado goNorte
    int cont; //Variable contador para el array
    int es;

    do{
        printf("\nTe encuentras en:"); 	
        impEstado(actual);
        new_estado(array[cont], actual);
        cont++;
    }while(cont<=5);
    return 0;
}

void new_estado(int car, int edo2){
    Estados edo_sig;
    switch(edo2){
        //Movimientos para go_Norte
        case 0:
            switch(car){
                case 0:
                    edo_sig = goNorte;
                    break;
                case 1:
                    edo_sig = waitNorte;
                    break;
                case 2:
                    edo_sig = goNorte;
                    break;
                case 3:
                    edo_sig = waitNorte;
                    break;
            }
            break;
        //Movimientos para wait_Norte
        case 1:
            edo_sig = goEste;
            break;
        //Movimientos para go_Este
        case 2:
            switch(car){
                case 0:
                    edo_sig = goEste;
                    break;
                case 1:
                    edo_sig = goEste;
                    break;
                case 2:
                    edo_sig = waitEste;
                    break;
                case 3:
                    edo_sig = waitEste;
                    break;
            }
            break;
        //Movimientos para wait_Norte
        case 3:
            edo_sig = goNorte;
            break;
    }
    actual=edo_sig;   ///Declaramos al estado actual como el siguiente estado
}
//m�todo para imprimir el Estado
void impEstado(int edo){
    switch(edo){
        case 0:
            printf("goNorte");
            break;
        case 1:
            printf("waitNorte");
            break;
        case 2:
            printf("goEste");
            break;
        case 3:
            printf("waitEste");
            break;
    }
}
